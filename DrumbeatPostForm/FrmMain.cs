﻿using DrumbeatPostForm.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using SQLBuilder.Repositories;
using DrumbeatPostForm.Entity;
using System.Diagnostics;

namespace DrumbeatPostForm
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        SqliteRepository db = DBHelper.GetDB();//new SqliteRepository(@"Data Source=C:\Users\Administrator\Desktop\LoclData.db;Version=3;");

        private delegate void txtHandeler(object obj);

        private void FrmMain_Load(object sender, EventArgs e)
        {
            db.FindList<UrlEntity>().ToList().ForEach(x =>
            {
                cboUrl.Items.Add(x.Url);
            });
        }

        private void btnPost_Click(object sender, EventArgs e)
        {
            btnPost.Enabled = false;
            Thread thread = new Thread(ExcuteThreads);
            thread.Start(sender);
          
        }

        private void ExcuteThreads(object obj)
        {
            txtHandeler handeler = new txtHandeler(HandlerPost);
            this.Invoke(handeler, new object[] { obj });
        }
        private void HandlerPost(object obj)
        {
            /*计时器*/
            Stopwatch sw = new Stopwatch();
            sw.Start();

            txtReturnValue.Text = JsonHelper.ConvertJsonString(PostHelper.Post(cboUrl.Text, txtparameter.Text));

            sw.Stop();
            lblTime.Text = $"{sw.ElapsedMilliseconds} ms";

            /*计时器*/

            btnPost.Enabled = true;
            var entity = db.FindEntity<UrlEntity>(x => x.Url == cboUrl.Text);
            if (entity == null)
            {
                db.Insert(new UrlEntity()
                {
                    Url = cboUrl.Text,
                    CreateDate = DateTime.Now,
                    Id = Guid.NewGuid().ToString(),
                    Parameter = txtparameter.Text,
                    ReturnData = txtReturnValue.Text,
                    Type = 1
                });
                cboUrl.Items.Add(cboUrl.Text);
            }
            else
            {
                entity.Parameter = txtparameter.Text;
                entity.ReturnData = txtReturnValue.Text;
                entity.ModifyDate = DateTime.Now;
                db.Update(entity);
            }

        }

        private void cboUrl_SelectedIndexChanged(object sender, EventArgs e)
        {
            var entity = db.FindEntity<UrlEntity>(x => x.Url == cboUrl.Text);
            txtReturnValue.Text = entity?.ReturnData;
            txtparameter.Text = entity?.Parameter;
        }

        private void txtCliperToParameter_Click(object sender, EventArgs e)
        {
            txtparameter.Text = txtCipher.Text.Trim()==""? txtparameter.Text : txtCipher.Text;
        }
        private void btnPlainToParameter_Click(object sender, EventArgs e)
        {
            txtparameter.Text = btnJsonToClass.Text.Trim()==""? txtparameter.Text : btnJsonToClass.Text.Trim();
        }
        private void btnDeleteUrl_Click(object sender, EventArgs e)
        {
            db.Delete<UrlEntity>(x => x.Url == cboUrl.Text);
            cboUrl.Items.Remove(cboUrl.Text?.ToString());
            cboUrl.Text = "";
            db.ExecuteBySql("VACUUM");
        }

        private void btnEnCode_Click(object sender, EventArgs e)
        {
            //加密
            txtCipher.Text= Base64Helper.Base64Encode(btnJsonToClass.Text);
        }

        private void btnDeCode_Click(object sender, EventArgs e)
        {
            //解密
            btnJsonToClass.Text = JsonHelper.ConvertJsonString(Base64Helper.Base64Decode(txtCipher.Text));
        }

        private void cbTop_CheckedChanged(object sender, EventArgs e)
        {
            if (cbTop.CheckState== CheckState.Checked)
                this.TopMost = true;
            else
                this.TopMost = false;
        }

        private void btnGet_Click(object sender, EventArgs e)
        {
            txtReturnValue.Text= JsonHelper.ConvertJsonString( PostHelper.Get(cboUrl.Text, ""));
            var entity = db.FindEntity<UrlEntity>(x => x.Url == cboUrl.Text);
            if (entity == null)
            {
                db.Insert(new UrlEntity()
                {
                    Url = cboUrl.Text,
                    CreateDate = DateTime.Now,
                    Id = Guid.NewGuid().ToString(),
                    Parameter = txtparameter.Text,
                    ReturnData = txtReturnValue.Text,
                    Type = 0
                });
                cboUrl.Items.Add(cboUrl.Text);
            }
            else
            {
                entity.Parameter = txtparameter.Text;
                entity.ReturnData = txtReturnValue.Text;
                entity.ModifyDate = DateTime.Now;
                db.Update(entity);
            }
        }

        private void btnJsonToCSharpClass_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (txtReturnValue.Text.Trim()=="")
            {
                MessageBox.Show("没有返回值转个锤子转。");
            }
            else
            {
                MessageBox.Show(@"在VS里面新建一个类
并将内容清空，然后复制json字符串
点击VS工具栏的【编辑】-【选择性粘贴】-【将json粘贴为类】", "据说很多人都不知道这个功能");
            }
        }

        private void btnToRig_Click(object sender, EventArgs e)
        {
            //解密
            btnJsonToClass.Text = JsonHelper.ConvertJsonString(Base64Helper.Base64Decode(txtparameter.Text));
        }

        private void btnClearReturn_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            txtReturnValue.Text = "";
        }

        private void btnClearparameter_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            txtparameter.Text = "";
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start("https://gitee.com/handloong/DrumbeatPostForm");
            }
            catch (Exception )
            {
                MessageBox.Show("地址:https://gitee.com/handloong/DrumbeatPostForm");
            }
        }
    }
}
