﻿namespace DrumbeatPostForm
{
    partial class FrmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.label1 = new System.Windows.Forms.Label();
            this.cboUrl = new System.Windows.Forms.ComboBox();
            this.btnPost = new System.Windows.Forms.Button();
            this.btnGet = new System.Windows.Forms.Button();
            this.txtparameter = new System.Windows.Forms.RichTextBox();
            this.txtReturnValue = new System.Windows.Forms.RichTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblTime = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnJsonToCSharpClass = new System.Windows.Forms.LinkLabel();
            this.cbTop = new System.Windows.Forms.CheckBox();
            this.btnDeleteUrl = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtCliperToParameter = new System.Windows.Forms.Button();
            this.btnPlainToParameter = new System.Windows.Forms.Button();
            this.btnDeCode = new System.Windows.Forms.Button();
            this.btnEnCode = new System.Windows.Forms.Button();
            this.txtCipher = new System.Windows.Forms.RichTextBox();
            this.btnJsonToClass = new System.Windows.Forms.RichTextBox();
            this.btnToRig = new System.Windows.Forms.Button();
            this.btnClearReturn = new System.Windows.Forms.LinkLabel();
            this.btnClearparameter = new System.Windows.Forms.LinkLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "URL";
            // 
            // cboUrl
            // 
            this.cboUrl.FormattingEnabled = true;
            this.cboUrl.Location = new System.Drawing.Point(40, 15);
            this.cboUrl.Name = "cboUrl";
            this.cboUrl.Size = new System.Drawing.Size(591, 20);
            this.cboUrl.TabIndex = 1;
            this.cboUrl.SelectedIndexChanged += new System.EventHandler(this.cboUrl_SelectedIndexChanged);
            // 
            // btnPost
            // 
            this.btnPost.Location = new System.Drawing.Point(643, 15);
            this.btnPost.Name = "btnPost";
            this.btnPost.Size = new System.Drawing.Size(38, 23);
            this.btnPost.TabIndex = 3;
            this.btnPost.Text = "Post";
            this.btnPost.UseVisualStyleBackColor = true;
            this.btnPost.Click += new System.EventHandler(this.btnPost_Click);
            // 
            // btnGet
            // 
            this.btnGet.Location = new System.Drawing.Point(684, 15);
            this.btnGet.Name = "btnGet";
            this.btnGet.Size = new System.Drawing.Size(41, 23);
            this.btnGet.TabIndex = 4;
            this.btnGet.Text = "Get";
            this.btnGet.UseVisualStyleBackColor = true;
            this.btnGet.Click += new System.EventHandler(this.btnGet_Click);
            // 
            // txtparameter
            // 
            this.txtparameter.Location = new System.Drawing.Point(10, 85);
            this.txtparameter.Name = "txtparameter";
            this.txtparameter.Size = new System.Drawing.Size(776, 37);
            this.txtparameter.TabIndex = 6;
            this.txtparameter.Text = "";
            // 
            // txtReturnValue
            // 
            this.txtReturnValue.Location = new System.Drawing.Point(10, 147);
            this.txtReturnValue.Name = "txtReturnValue";
            this.txtReturnValue.Size = new System.Drawing.Size(777, 414);
            this.txtReturnValue.TabIndex = 7;
            this.txtReturnValue.Text = "";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.btnClearparameter);
            this.groupBox1.Controls.Add(this.btnClearReturn);
            this.groupBox1.Controls.Add(this.btnToRig);
            this.groupBox1.Controls.Add(this.lblTime);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.btnJsonToCSharpClass);
            this.groupBox1.Controls.Add(this.cbTop);
            this.groupBox1.Controls.Add(this.btnDeleteUrl);
            this.groupBox1.Controls.Add(this.txtReturnValue);
            this.groupBox1.Controls.Add(this.cboUrl);
            this.groupBox1.Controls.Add(this.txtparameter);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnPost);
            this.groupBox1.Controls.Add(this.btnGet);
            this.groupBox1.Location = new System.Drawing.Point(4, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(795, 567);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.ForeColor = System.Drawing.Color.Red;
            this.lblTime.Location = new System.Drawing.Point(49, 45);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(29, 12);
            this.lblTime.TabIndex = 12;
            this.lblTime.Text = "00ms";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 45);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 12);
            this.label6.TabIndex = 11;
            this.label6.Text = "耗时:";
            // 
            // btnJsonToCSharpClass
            // 
            this.btnJsonToCSharpClass.AutoSize = true;
            this.btnJsonToCSharpClass.Location = new System.Drawing.Point(82, 128);
            this.btnJsonToCSharpClass.Name = "btnJsonToCSharpClass";
            this.btnJsonToCSharpClass.Size = new System.Drawing.Size(125, 12);
            this.btnJsonToCSharpClass.TabIndex = 10;
            this.btnJsonToCSharpClass.TabStop = true;
            this.btnJsonToCSharpClass.Text = "将Json转换成C#实体类";
            this.btnJsonToCSharpClass.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnJsonToCSharpClass_LinkClicked);
            // 
            // cbTop
            // 
            this.cbTop.AutoSize = true;
            this.cbTop.Location = new System.Drawing.Point(96, 41);
            this.cbTop.Name = "cbTop";
            this.cbTop.Size = new System.Drawing.Size(48, 16);
            this.cbTop.TabIndex = 9;
            this.cbTop.Text = "置顶";
            this.cbTop.UseVisualStyleBackColor = true;
            this.cbTop.CheckedChanged += new System.EventHandler(this.cbTop_CheckedChanged);
            // 
            // btnDeleteUrl
            // 
            this.btnDeleteUrl.Location = new System.Drawing.Point(729, 16);
            this.btnDeleteUrl.Name = "btnDeleteUrl";
            this.btnDeleteUrl.Size = new System.Drawing.Size(33, 23);
            this.btnDeleteUrl.TabIndex = 8;
            this.btnDeleteUrl.Text = "删除";
            this.btnDeleteUrl.UseVisualStyleBackColor = true;
            this.btnDeleteUrl.Click += new System.EventHandler(this.btnDeleteUrl_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtCliperToParameter);
            this.groupBox2.Controls.Add(this.btnPlainToParameter);
            this.groupBox2.Controls.Add(this.btnDeCode);
            this.groupBox2.Controls.Add(this.btnEnCode);
            this.groupBox2.Controls.Add(this.txtCipher);
            this.groupBox2.Controls.Add(this.btnJsonToClass);
            this.groupBox2.Location = new System.Drawing.Point(804, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(188, 567);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Base64操作";
            // 
            // txtCliperToParameter
            // 
            this.txtCliperToParameter.Location = new System.Drawing.Point(12, 454);
            this.txtCliperToParameter.Name = "txtCliperToParameter";
            this.txtCliperToParameter.Size = new System.Drawing.Size(75, 33);
            this.txtCliperToParameter.TabIndex = 5;
            this.txtCliperToParameter.Text = "←密文参数";
            this.txtCliperToParameter.UseVisualStyleBackColor = true;
            this.txtCliperToParameter.Click += new System.EventHandler(this.txtCliperToParameter_Click);
            // 
            // btnPlainToParameter
            // 
            this.btnPlainToParameter.Location = new System.Drawing.Point(12, 416);
            this.btnPlainToParameter.Name = "btnPlainToParameter";
            this.btnPlainToParameter.Size = new System.Drawing.Size(75, 32);
            this.btnPlainToParameter.TabIndex = 5;
            this.btnPlainToParameter.Text = "←明文参数";
            this.btnPlainToParameter.UseVisualStyleBackColor = true;
            this.btnPlainToParameter.Click += new System.EventHandler(this.btnPlainToParameter_Click);
            // 
            // btnDeCode
            // 
            this.btnDeCode.Location = new System.Drawing.Point(93, 454);
            this.btnDeCode.Name = "btnDeCode";
            this.btnDeCode.Size = new System.Drawing.Size(27, 33);
            this.btnDeCode.TabIndex = 4;
            this.btnDeCode.Text = "↑";
            this.btnDeCode.UseVisualStyleBackColor = true;
            this.btnDeCode.Click += new System.EventHandler(this.btnDeCode_Click);
            // 
            // btnEnCode
            // 
            this.btnEnCode.Location = new System.Drawing.Point(126, 454);
            this.btnEnCode.Name = "btnEnCode";
            this.btnEnCode.Size = new System.Drawing.Size(29, 33);
            this.btnEnCode.TabIndex = 3;
            this.btnEnCode.Text = "↓";
            this.btnEnCode.UseVisualStyleBackColor = true;
            this.btnEnCode.Click += new System.EventHandler(this.btnEnCode_Click);
            // 
            // txtCipher
            // 
            this.txtCipher.Location = new System.Drawing.Point(6, 493);
            this.txtCipher.Name = "txtCipher";
            this.txtCipher.Size = new System.Drawing.Size(176, 66);
            this.txtCipher.TabIndex = 2;
            this.txtCipher.Text = "";
            // 
            // btnJsonToClass
            // 
            this.btnJsonToClass.Location = new System.Drawing.Point(2, 20);
            this.btnJsonToClass.Name = "btnJsonToClass";
            this.btnJsonToClass.Size = new System.Drawing.Size(182, 390);
            this.btnJsonToClass.TabIndex = 1;
            this.btnJsonToClass.Text = "";
            // 
            // btnToRig
            // 
            this.btnToRig.Location = new System.Drawing.Point(731, 61);
            this.btnToRig.Name = "btnToRig";
            this.btnToRig.Size = new System.Drawing.Size(55, 23);
            this.btnToRig.TabIndex = 13;
            this.btnToRig.Text = "→";
            this.btnToRig.UseVisualStyleBackColor = true;
            this.btnToRig.Click += new System.EventHandler(this.btnToRig_Click);
            // 
            // btnClearReturn
            // 
            this.btnClearReturn.AutoSize = true;
            this.btnClearReturn.Location = new System.Drawing.Point(11, 128);
            this.btnClearReturn.Name = "btnClearReturn";
            this.btnClearReturn.Size = new System.Drawing.Size(65, 12);
            this.btnClearReturn.TabIndex = 14;
            this.btnClearReturn.TabStop = true;
            this.btnClearReturn.Text = "清空返回值";
            this.btnClearReturn.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnClearReturn_LinkClicked);
            // 
            // btnClearparameter
            // 
            this.btnClearparameter.AutoSize = true;
            this.btnClearparameter.Location = new System.Drawing.Point(9, 66);
            this.btnClearparameter.Name = "btnClearparameter";
            this.btnClearparameter.Size = new System.Drawing.Size(53, 12);
            this.btnClearparameter.TabIndex = 15;
            this.btnClearparameter.TabStop = true;
            this.btnClearparameter.Text = "清空参数";
            this.btnClearparameter.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnClearparameter_LinkClicked);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(683, 124);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(23, 22);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label4.Location = new System.Drawing.Point(583, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 12);
            this.label4.TabIndex = 16;
            this.label4.Text = "软件开放源代码";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1004, 573);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PostWoman   -- https://www.52pojie.cn/    --邓振振  -2018-12-19";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboUrl;
        private System.Windows.Forms.Button btnPost;
        private System.Windows.Forms.Button btnGet;
        private System.Windows.Forms.RichTextBox txtparameter;
        private System.Windows.Forms.RichTextBox txtReturnValue;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox btnJsonToClass;
        private System.Windows.Forms.RichTextBox txtCipher;
        private System.Windows.Forms.Button btnEnCode;
        private System.Windows.Forms.Button btnDeCode;
        private System.Windows.Forms.Button btnPlainToParameter;
        private System.Windows.Forms.Button txtCliperToParameter;
        private System.Windows.Forms.Button btnDeleteUrl;
        private System.Windows.Forms.CheckBox cbTop;
        private System.Windows.Forms.LinkLabel btnJsonToCSharpClass;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Button btnToRig;
        private System.Windows.Forms.LinkLabel btnClearReturn;
        private System.Windows.Forms.LinkLabel btnClearparameter;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label4;
    }
}

