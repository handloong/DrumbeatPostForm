﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLBuilder.Repositories;

namespace DrumbeatPostForm.Entity
{
    public static class DBHelper
    {
        public static SqliteRepository GetDB()
        {
            SqliteRepository db;
            var dbFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "localData.db");
            if (!File.Exists(dbFile))
            {
                File.WriteAllText(dbFile, string.Empty);
                db = new SqliteRepository($@"Data Source={dbFile};Version=3;");
                db.ExecuteBySql(@"
CREATE TABLE UrlEntity ( 
Id         TEXT PRIMARY KEY,
Url        TEXT,
Parameter  TEXT,
ReturnData TEXT,
Type       INT,
CreateDate DATE,
ModifyDate DATE 
);");
            }
            else
            {
                db = new SqliteRepository($@"Data Source={dbFile};Version=3;");
            }
            return db;
        }
    }
}
