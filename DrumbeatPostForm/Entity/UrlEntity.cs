﻿using SQLBuilder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrumbeatPostForm.Entity
{
    [Table("UrlEntity")]
    public class UrlEntity
    {
        [Key]
        public string Id { get; set; }
        public string Url { get; set; }
        public string Parameter { get; set; }
        public string ReturnData { get; set; }
        /// <summary>
        /// 1 post  0 get
        /// </summary>
        public int ? Type { get; set; }

        public DateTime? CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }
    }
}
